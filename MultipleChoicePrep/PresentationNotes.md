## 03/13/23
### Presentation 1

* Understand instructions
* Understand the 5 big ideas
* Study 5 big ideas seperatly
* Most important is studying and practice
* Khan academy has a good resource for studying, but is in javascript
* Uset apcsp test guides website for practice tests

## 03/15/23
### Practice test 4 from 
1.  An automobile company uses GPS technology to implement a smartphone app for its customers that they can use to call their self-driving electric cars to their location. When the app is used, the coordinates of the car owner are sent to their car. The car then proceeds to drive itself from the parking lot to its owner. What is a possible concern for user privacy when using this app?  
- [ ] Idling periods for which the car is running in anticipation of the signal would cause excess fuel consumption
- [ ] Unauthorized access to vehicles through the app
- [x] User location data could be used by companies and third-party stakeholders
- [ ] Interference of signals if two people use the app simultaneously and close by

2. An organization needs to process large amounts of data that cannot be handled by the current PC setup and has extra PC devices lying around in the storage room. What sort of connection should this organization consider to improve its processing capabilities?
- [ ] The organization should consider maintaining their sequential computing model
- [x] The organization should consider using a distributed computing model
- [ ] The organization should consider limiting the data collection requirements
- [ ] The organization should consider adding additional servers

3. A programmer has developed an algorithm that computes the sum of integers taken from a list. However, the programmer wants to modify the code to sum up even numbers from the list. What programming structure can the programmer add to do this?
- [ ] Sequencing
- [ ] Iteration
- [x] Selection
- [ ] Searching
