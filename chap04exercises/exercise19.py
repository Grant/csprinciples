earnings_per_week = 20
earnings_per_month = earnings_per_week * 4
final_earnings = 200
months = final_earnings / earnings_per_month
print(f"It will take {months} months to earn {final_earnings} if you make {earnings_per_week} dollars a week.")
