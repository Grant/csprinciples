## Bitwise Not: What is it?
- for each input bit that is 1, the output is 0. Input bits that are 0 will be output as 1.
- ex: 0 0 0 becomes a 1 1 1 and vice versa
- The operator for a bitwise not is "~".

## What it does
**Process**
1. it starts with the number given
2. make number negative
3. inverts all of the bits 1 to 0 and 0 to 1
4. add 1 to the whole number
5. flip bits back

**Example**
 1. start with 5 or 0101 in bin
 2. make it negative -0101
 3. switch all bits to -1010
 4. add 1 to make -1001
 5. flip bit back -0110
 6. so, ~5 = -6

## Practical Application
- General uses for various Bitwise operators include encryption, compression, graphics, communications over ports/sockets, embedded systems programming and finite state machines
