# Chapter 3: Names for Number Notes
## Assinging a Name
Computer can associate a name with a value. This is done by creating a variable.
A variable is a space in computer memory that can represent a value. One example is a score in a video game, which usualy starts at zero and can increase or decrease as you play. You can also associate a name with a variable, like when you make a new contact for someone on your phone. A variable is just a group of data that is labled and can store data. This value is anything that can be represend in a computer's memory, which is made up of numbers. Everything in a computer's memory is translated into these numbers.
In programing, setting a variable is an assingment. An assingment can be formated like \(String\) = \(Number\). Say the string was g and the number was 7, then we would do g = 7. If we want to change the number to 4 later, then we can do g = 4. The right hand side has a value and we are assinging the value a name \(4 has the name of g\)
Name have rules they need to folow. They need to:
- Start with a letter \(g\) or an underscore \(\_\)
- If it has numbers, they cannot be the first digit.
- It cant be a python keyword \(and, def, elif, else, for, if, import, in, not, or, return, while\) because they have a special meaning in python and are part of the language.
- Ensure that uppercases are used properly
- Not have spaces
## Expresions
The right hand of an assignment statement does not have to be an value. It can be and arithmetic expression. The eprexxion will be evaluated and then the value will be stored instead of the expression \(a = 4 * 4 will be saved as 16, not 4 * 4\). Any standard mathematical symbols can be used, and som symbols that are much less common like \%.
## Expression types
| Expression | Meaning        |
| ---------- | -------------- |
| a + b      | Addition       |
| a \- b     | Subtraction    |
| a * b      | Multiplication |
| a \/ b     | Division       |
| a \/\/ b   | Floor Division |
| a \% b     | Modulo         |
| -a         | Negation       |

## How Expressions are Evaluated 
Negation takes priority, followed by multiplication, division and modulo. The last step will be addition and subtraction. For symbols of the same priority they are evaluated left to right, just like standard math. Also like standard math, the second to the top priotity is parentheseses, which can be used to change the order.
