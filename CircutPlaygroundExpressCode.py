from adafruit_circuitplayground.express import cpx
import random

player_position = 2
enemy_position = 7
enemy_direction = random.randint(1, 2)
player_has_moved = False

for a in range(10):
        cpx.pixels[a] = (220, 220, 220)
cpx.pixels[player_position] = (0, 0, 255)
cpx.pixels[enemy_position] = (255, 0, 0)

while True:
    cpx.pixels.brightness = 0.2
    if cpx.button_a:
        if player_position != 0:
            player_position -= 1
        else:
            player_position = 9
        player_has_moved = True
    elif cpx.button_b:
        if player_position != 9:
            player_position += 1
            player_has_moved = True
        else:
            player_position = 0
            player_has_moved = True
    if player_has_moved:
        if player_position == enemy_position:
            for a in range(10):
                cpx.pixels[a] = (255, 0, 0)
            break
        if random.randint(1, 10) == 1:
            if enemy_direction == 2:
                enemy_direction = 1
            else:
                enemy_direction = 2
        if enemy_direction == 2:
            if enemy_position != 0:
                enemy_position -= 1
            else:
                enemy_position = 9
        else:
            if enemy_position != 9:
                enemy_position += 1
            else:
                enemy_position = 0
        player_has_moved = False
        if player_position == enemy_position:
            for a in range(10):
                cpx.pixels[a] = (255, 0, 0)
                break
        for a in range(10):
            cpx.pixels[a] = (220, 220, 220)
        cpx.pixels[player_position] = (0, 0, 255)
        cpx.pixels[enemy_position] = (255, 0, 0)

