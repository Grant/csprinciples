* code segment
  - A group of lines of code that does something for the program.
* collaboration
  - Multipule people working together on something.
* comments
  - Thing that let someone who is reading the cod know what diferent segments of the code do.
* debugging
  - Removing unintentional error from code.
* event-driven programming
  - A computer program writen to respond to inputs.
* incremental development process
  - A development process that is broken up into smaller parts that work fully.
* iterative development process
  - A development process that breaks up the development of a larger product into smaller steps
* logic error
  - A bug that causes the program to work incorectly.
* overflow error
  - An error that the program gives if it has to process too much data, leading to the program crashing.
* program
  - Something writen in code that does something.
* program behavior
  - What the program does.
* program input
  - Something that the program takes and does something based on what input is given.
* program output
  - What the program's end result is based on the program input.
* prototype
  - A version of something that is not released and is used for testing.
* requirements
  - Parameter for what something must do.
* runtime error
  - An error where the program remains running even if it's task is completed.
* syntax error
  - An error that is caused by a mistake in formating.
* testing
  - Seeing if something has errors that would need revisions.
* user interface
  - Something that lets the user interact with the program. Can be text-based or graphical.
