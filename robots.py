from gasp import *
import random

begin_graphics()
Finished = False

class Robot:
    pass
class Player:
    pass
level = input("What level would you like? max level is 10. - ")

def collided(player, robots):
    for robot in robots:
        if player_x == robot_x and player_y == robot_y:
            return True
    return False

def place_player():
    global player_x, player_y, player_location
    player = Player()
    player_x = random.randint(0, 63)
    player_y = random.randint(0, 47)
    player_location = Circle((10 * player_x + 5, 10 * player_y + 5), 5, filled = True)
def num_of_robots():
    global num_of_robots, level_number
    level_number = int(level)
    num_of_robots = 2 ** level_number
    return num_of_robots

def level_num():
    level_number = int(level)
    return int(level_number)

def place_robots():
    global robots
    robots = []
    while len(robots) < num_of_robots():
        robot = Robot()
        robot_x = random.randint(0, 63)
        robot_y = random.randint(0, 47)
        if not collided(robot, robots):
            robot.r = Box((10 * robot.x + 5, 10 * robot.y + 5), 10, 10, filled = False)
            robots.append(robot)

def move_robots():
    global robots, player
    for robot in robots:
        if robot_x < player_x:
            robot_x += 1
        elif robot_x > player_x:
            robot_x -= 1
        if robot_y < player_y:
            robot_y += 1
        elif robot_y > player_y:
            robot_y -= 1
        move_to(robot.r, (robot_x * 10 + 5, robot_y * 10 + 5))



def move_player():
    global player_x, player_y, player_location
    key = update_when('key_pressed')
    if key == '8' and player_y < 47:
        player_y += 1
    elif key == 'k' and player_y > 0:
        player_y -= 1
    if key == 'u' and player_x > 0:
        player_x -= 1
    elif key == 'o' and player_x < 63:
        player_x += 1
    if key == '7' and player_x > 0 and player_y < 47:
        player_x -= 1 
        player_y += 1
    elif key == '9' and player_x < 63 and player_y < 47:
        player_x += 1
        player_y += 1
    elif key == 'j' and player_x > 0 and player_y > 0:
        player_x -= 1
        player_y -= 1
    elif key == 'l' and player_x < 63 and player_y > 0:
        player_x += 1
        player_y -= 1
    if key == 'i':
        player_x = random.randint(0, 63)
        player_y = random.randint(0, 47)
    move_to(player_location, (10 * player_x + 5, 10 * player_y + 5))
    move_robots()


while Finished == False:
    if level_num() > 10:
        Finished = True
    elif len(robots) < 0:
        move_player()
    elif len(robots) == 0:
        Text("You beat the level! Press any key when you are ready for the next one!", (300, 200), size = 10)
        update_when('key_pressed')
        level_number += 1
    if Finished == True:
        Text("You beat the game! Press any key when you want to end the program!", (300, 200), size = 15)
        update_when('key_pressed')
        end_graphics()
