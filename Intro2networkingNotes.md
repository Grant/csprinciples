## Chapter 01 notes

* While the Internet seems fairly simple from a user perspective, it took over 20 years of research to create the first vesion of it, and it is actualy very complex
* Before the internet, people were connected with wires, speakers, and microphones called "tellephones". They would have to go through an opperator that would conect the wires of 2 people together so that they could talk to each other. This worked well when the physical distance between the two phones was not very large, but issues would arise when the physical distance was large.
* For longer distances, the diferent opperator centers would have wires between them that could make the long-distance calls posible. Long distance calls were quite expensive because the phone companies could only have very few going on at a time. This changed with the invention and switch to fiber-optic wires.
* Computers comunicat in a similar way. They send messages that can be short, medium, or long. When sending messages between computers, there would be a cue of messages to be sent. Conecting computers over long distances, similarly to phones, was quite expensive, for the same reason long distance phone calls were expensive. The most consistent connection was when both computers were of the same brand.
* Later on, people figured out that if one computer was connected to another computer, and that computer was connected to another computer, and so on in a chain, then messages could be sent over long distances without the long distance wire problem would be eliminated. However, this could take a long time, but was still quicker and cheaper than other communication messages.
* To increase the spead that messages could be sent, messages would get broken  up into smaller fragments. These are called "packets". It was pionered in the 1960s, but was not used until the 80s because the networks were not sophisticated enough to handle the packet systems. This system made it so that short messages did not have to wait for long messages to completley finish sending. 
* Once sending messages between computers was popularized, computers were devolped that were specialized to send messages between computers, called "Interface Message Procesors", or "IPMs". The would later be called "Routers". These made it much simpiler to send and recieve messages from computers.
* To know what computer messages were suposed to be sent to in the early days of sending messages between computers, it was nessacary for each computer to have it's own unique "Adress". To know what computer the message needed to be sent to, the adress would be included so that the message did not go the wrong place. This adress would also let the computer sending the message to determine what route would be fastest so that the message reaches it's destination. For large messages with packets, each packet would take a diferent rout so that it would be the most efficent way.
* These all combine to make the internet. The internet is just a large network of coneected computers and routers using all these systems.
### Vocabulary
* adress
* hop
* LAN (Local Area Network)
* leased line
* operator (telephone)
* packet
* router
* store-and-foreward network
* WAN (Wide Are Network)

## Chapter 02 notes

* The modern internet network has an architecture with 4 parts: "Application", "Transport", "Internetwork", and "Link". They can be visuilized in this order:
----------------
| Application  |
----------------
|  Transport   |
----------------
| Internetwork |
----------------
|     Link     |
----------------
* Link deals with the connection between your computer and the LAN, and Application is what the user interacts with. This model is refered to as the "TCP/IP Model", whic his reference to the Transport Control Protocal "TCP", which is used to implement the Transport layer and Internet Protocal "IP".
 
### Link layer

* The Link layer is responsible for conecting the computer to the LAN and sending data in a single Hop. The most common technology for this today is wireless networking. When using a device that is wireless, the device is only sending data a limited distance. All devices that are conected to some sort of wifi or celular data conect over a relatively short distance. 
* The Link layer needs to do two things. The firs thing it needs to do is encrypt the sent data. For this, engineers need to agree on the radio wave frequencees, voltage, or fligh frequency depending on what is used to send the data. They laso need to figure out how to divide the bandwidth among computers, so that recivers dont recieve a cluttered mess because of data colision. 
* Breaking data into passages makes this sharing easier. Depending on how many computers are using the network, the frequency that packets are sent changes to make network use fair. 
* The way computers know if another computer is sending data at the same time is a solution called "Carrier Sense Multipule Access with Collision Detection", or "CSMA/CD". Contrased to the name, the concept is simple. When sending data, the computer fist check if another computer on the network is sending data, and if data is not being sent, it waits. If data starts being sent by 2 computers, the computers pause, wait for diferent lengths of time, and the atempt to send the data again.
* When a computer finishes sending a packet, it pauses to  allow wating computers to send data. The computer waiting will then detect that the other computer has finished sending the packet, and will then start sending it's packet. This works very well no matter how many computers are on the network ateampting to send data.
* Some Link layers (Ex. cellular data, wifi connection, motom, ect) are shared conections and need CSMA/CD to ensure fair use. 

### The Internetwork layer (IP)

* Once a packet makes it's first hop, it will be in a router. The packet has the source and destination adress, and the router looks at the destination adress to determine the fastest route to the destination. The problem is that the router does not know the location of every router, so it makes it's best guess at which router will get it closer to its destination, and sends it there. As the packet gets closer, the routers get more accurate when it comes to sending the packet. Once the packet reaches the destination, the Link layer knows what to do with it.
* If a problem occurs on the determined rout, the packet is rerouted to the next best option. Sometimes ackets can get lost, and that is why the Transport layer exsists.

### The Transport layer (TCP)

Errors can occur when packets get delivered, such as a packet getting lost or packets arriving out of order. Allongside the source and destination adress, packets contain a offset for where the packet "fits" in the message. This allows the original message to be reconstructed, even if packets are recieved out of order. When reconstructing the message, the reciving computer periodicaly sends data to the source computer. If there is a packet missing, the source computer requests to resend that packet. The source computer keeps a copy of the origonal message until the recieving computer agknoledges that it has recieved all the packets, after which the source computer discards the copy.

### The Application layer 

* Once the previous layers had been constructed, the question then became what could use this network. It started with file and message transfering, but more and more applications started using this network. Now, the "internet" is the most popular application of these methods. 
* Applications generaly have 2 haves to them. There is the "server" half, which recieves connections, and there is the "client" half, which makes connections. The web browser shows "Uniform Resource Loaders", or "URLs", are the servers that the client is connecting to. When these two halves are developed, we must also define an "application protocal" for how these two halves interact with each other. These are quite diverse and depend on the task of the application.

### Stacking the layers

* The reason the layers are stacked in the order they are in is because each layer makes use of the layers above and below it to complete it's task. All 4 layers run in the source and destination computer, and the user only ever interacts with the application layer. Routers have no understanding of the Transport or Application layers, because they have no use for them. They only use the internetwork and link layers. When writing a networked application, one would most often interact with the Transport layer. 

### Vocabulary

* client
* fiber optic
* offset
* server
* window size

## Chapter 03

* The link layer is on the bottom of the stack because it is the closest to the physical network media. A key part of it is that, most of the time, data can only be sent partway to the detination. This can be via copper or fiber-optic cables, cellular data, or even satalites. While data can be sent very long distances with the link layer, in the end it is only one hop, which is often not enough to get to the final destination. Wifi is a great example of what problems are solved by the link layer.
* When conected to the internet, a device uses a small radio, with a radius of about 300 meters. This radio connects to a router or cell tower. Most of the time, the first hop is from the raio to the first router along the path, which is sometimes refered to as the "base station" or "gateway".
* If there are multipule computers connected to a router, they all recieve all packets sent to a computer on that router, even if the packet is not adressed to your computer. They also "hear" packets being sent by the other computers, so they know which packets to ignore. Unfortunatley, this means that a rouge computer on the network could steal packets adressed to other computers containing personal information, like passwords or other account info.
* Every wifi radio (Computer, phone, router) was assigned a number when it was manufactured. It can be shown as "0f:2a:b3:lf:b3:la", but it is just a way of representing the 48-bit "Media Access Control", or "MAC", address which is another name for a the serial number assigned. This is how computers know the source and destination computer of a packet. When first connecting to a network, a computer sends a message, with it's own MAC adress as the source, asking what the MAC adress of the network is. The network then responds with another message to the source computer, with the source adress being the networks, telling the computer the MAC adress of the network.
* Similaryly to a crowded room, computers on 1 network can't all send messages at the same time. All of the computers are using the same radio frequency, so the message would get lost. There are techniques to avoid this, with the first being called "carier sense". This is where the computer listens for other computers sending packets, and when it hears silence, it will send it's own packet. As a failsafe in case two computers start transmitting at the same time, the computer listens to see if it can hear itself. If it can't hear itself, it knows another computer is transmitting, so it stops. This is called "collision detection". The computer will then wait for a random amount of time to try again, ensuring that one computer WILL go first. The formal name for all of this is "Carier Sense Multipule Access with Colision Detection", or "CSMA/CD".
* When many stations want to transmit at the same time, one is given a "Token", and that is the only one autorized to transmit. Once that computer is done with the token, it will give it to a diferent computer.

### Vocabulary

* base station
* brodcast
* gateway
* MAC adress
* token

## Chapter 04

* The internetworking layer is in charge of getting the data to it's final destination, after the first hop. To get there, the data needs to make many hops across many networks. To do this, the data hops across many diferent parts of the lik layer, like wifi, ethernet, fiber optic, and satalite. To get to the final destenation, unique route that it takes. While on it's route, it stops at routers where it has to make a decision on where to go next. These routers manage incoming and outgoing hops. The router ensures that the data makes the correct next hop. As opposed to the data making the decision on where to go next, the router makes that decision based on the destination adress. The router is able to quickly make the decision on which hop to take because every packet is marked with the destination "Internet Protocal", or IP, adress.
* Because it is imposible to know wheree the computer with a certain link layer adress is. Because of this, computers are assigned another value, the IP adress. There are 2 types of IP adresses. The "old" IPv4 adresses, and the "new" IPv6 adresses. The IPv4 adresses, with x representing a number, are formatted as follows:
* xxx.xxx.xxx.xxx
* The maximum number each group of 3 x's can be is 255, which is the maximum number that can be represented by 1 byte of data. Contrasted with this, IPv6 adresses are formatted as follows:
* xxxx.xxxx.xxxx.xxxx.xxxx.xxxx.xxxx.xxxx
* Instead of x representing a number, it represents a digit of hexidecimal, with the IPv6 adresses being 16 bytes of data. Notes will refer to IPv4 adresses
* IP adresses have 2 parts: The network number, which is the first 2 bytes, and the Host identifier, which is the second 2 bytes. The network number identifies what network the computer is curently on, and the host identifier is the spesific computer on that network. This makes it so that routers can rout to the network number, making the job of the router much easier and more efficient. 
* When a router is introduced to the network, asside from a few predetermined routes, it does not know how to route packets. Some packets come with predetermined routes, but some don't. For the ones that don't have a predetermined route, it asks its "neighbors"
* If, for some reason, the link between routers is broken, the router recieving packets must find a new way to get the packets to their destination. To do this, for routes going to a spesific network number that it can no longer get to, the router goes through the same discovery process as in first time settup, with the exclusion of the broken link. This makes it incredibly important that routers have more than 1 link to get to 1 destination. Because a router is always updating it's routing, once a broken link is back online, the router will find out and reroute through that link.
* When a packet is in transit, it's route is constantly changing and updating for efficiency. Computers have a command, "traceroute", that lets you get a pretty good assumption about the route that the packet is going to take. Sometimes, if routers are confused, packets headed to one location can get trapped in an infinite cycle of going around and around in a loop between 2 or more routers. To solve this, each packet is a value called "Time To Live", or TTL. Each time the packet makes a hop, that number is subtracted by 1. When that number reaches 0, the packet is thrown away, preventing this infinite loop from continuing. The router also tells the original computer that the packet was thrown away, and by which router. This also lets the traceroute command be more accurate.
* Because computers move from location to location and router to router, they constantly need new IP adresses. The protocol for this is called the "Dynamic Host Configuration Protocal", or DHCP. This outlines how, once a computer is conected to a network, it will ask if there is a coneection to the internet, what the IP adress of the router is, and what IP adress it should use. When the router replies, the computer is then given its temporary IP adress. Sometimes this goes wrong, and 2 computers get assigned the same IP adress. If this happens, that IP adress is terminated, and the computers are given a new one. However, this is very rare. Annother rare occurance is where the router does not respond with the IP adress that the computer should use. If that happens, on some opperating systems, the computer will assign itself an IP adress. However, there is very little that can be done with these self-assigned IP adresses
* There are some special network numbers. These numbers are called "non-routable adresses". These adresses are ones that can be used to connect on a local level, but will not be conected to the international network. The reason why these adresses still work is because the router is doing something called "Network Adress Translation", or "NAT". This is essentialy where the router replaces the adress with a routable one when sending and recieving packets for that adress. 

### Vocabulary

* core router
* DHCP
* edge router
* Host Identifier
* IP Address
* NAT
* Network Number
* packet vortex
* RIR
* routing tables
* Time To Live (TTL)
* traceroute
* two-connected network

## Chapter 05

* The domain name system lets you acces websites by their domain name, instead of their IP adresses, which is easier for humans to remember. This also fixes the problem of conecting to a changing adress, with domain names being static, unlike IP adresses. When a computer conects to a domain name, the first thing the computer does is find out what the IP adress of that domain name is at the moment. Once the computer finds the IP adress, it then connects to the IP adress. This makes it easy for computers with a domain name to move, because the IP adress that the domain name is conected to is updated when the IP adress changes. 
* Domain names are based on who "owns" the domain name. At the top of the higherarchy is the International Corporation for Assigned Network Names and Numbers, or ICANN. The ICANN choses people and orgonizations to assign top-level domains (TLDs) to, like .com, .org, and .edu, and makes that orginization manage that domains. ICANN also assigns 2-letter domains for countries that are also top-level, called Country Code Top-Level Domains, or ccTDLs. These let domain names identify as inside a spesific country, like "jp" for Japan, "us" for the United States, and many more.
* Once an orgonization is managing a domain, it can give out domain names on that domain to orginizations, called subdomains. These subdomains can make new subdomains within their subdomain. The further left you get in a domain name, the more spesific it becomes, and the further left, less spesific.

### Vocabulary

* DNS
* domain name
* ICANN
* registrar
* subdomain
* TDL

## Chapter 06

* While the internetworking layer is nearly perfect, packets do get lost or misrouted ocasionaly. The problem with this is that packets need to be reasembeled in the order they were sent, and so missrouting can be a problem. While the internetworking layer is mostly reliable, it is not 100% reliable, and because of this the transport layer was developed to solve anything that went wrong in the internetworking layer's process. 
* When packets are sent, they have 3 "headers" attached to them. They are called the Link Header, the IP Header, and, the most important one, the TCP Header. The TCP header contains the information of where in the data the packet belongs, for data reaseambaly. The transport uses the data of where the packet belongs to trivialize data reasembally, offsetting the packets based on that information when reasembaling the data, which ensures that the data is correct. 
* When sending data, packets can be lost. The transport layer deals with this problem as well. When the recieving computer recieves all the packets, it tells the source computer. However, if it does not recieve all the packets, it tells the source computer which packet is missing. To account for these missing packets, the source computer keeps a coppy of the origoninal data, only deleting it once it receives conformation that the recieving computer has all the data. 

### Vocabulary

* acknoledgement
* buffering
* listen
* port

## Chapter 07 notes

* The application layet is any networked application that the user interacts with, and is the only part of the network that the user interacts with. We interact with the application layer, and the application layer interacts with  the rest of the model on our behalf. 
* For a networked application to run, it must have a client and server portion. The server is seprate from the computer running the application, and holds the information the user wants to access. The clientportion connects with the server, and shows the user the infrormation they want by getting it from the server. 
* When computers and servers are connecting, they have precise protocals they follow when interacting with servers. Each web application has diferent protocals, and they are offten well-documented. Most web applications use the "HTTP" or "HTTPS" protocal. Is is often used because it is relatively simple, and very user-friendly. 

### Vocabulary

* HTML
* HTTP
* IMAP
* flow control
* socket
* status code
* telenet
* web browser
* web server

## Chapter 08

* Security and privacy is not a concern that is adressed by the 4 layers of the internet, because it did not start as one. Because of this, data is now encrypted and decrypted when it is sent and recieved. The main way to make data unusable by prying eyes is to make it apear as random letters to anyone and anything but the destenation, and data encryption and decryption uses a similar method. What we use now is asymetric key encrytion and decryption. There is a public key and a private key. Anyone can use the public key, and it encrypts the data, but only the reciever can use the private key, and it decrypts the data.
* Because network security was developed after our current network model, there was a sublayer that was added as to not break anything, called the Secure Socket Layer (SSL) or Transport Layer Security (TLS), as part of the transport layer. This layer encrypts data before it is sent, and broken down into packet. It does this after making a connection to a remote host, and asking for the data to be encrypted or decrypted. To tell the diference when using a browser, an https domain is encrypted, while an http is not. The https domains are ever so slightly slower, but it makes no noticable diference. 
* When using a public key to decrypt data, if it is a publick key from a well-known website, it checks with it's compendium of well-known public keys. If it is not in that compendium, it will warn you before the encryption and data transfer

### Vocabulary

* asymetric key
* certificate athourity
* ciphertext
* decrypt
* encrypt
* plain text
* private key
* public key
* shared secret
* SSL
* TLS
