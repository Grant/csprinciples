def colored_sq(t, d, a, c1, c2, c3, c4):
    t.color(c1)
    t.forward(d)
    t.left(a)
    t.color(c2)
    t.forward(d)
    t.left(a)
    t.color(c3)
    t.forward(d)
    t.left(a)
    t.color(c4)
    t.forward(d)
    t.left(a)
    t.color('black')

from turtle import *
space = Screen()
g = Turtle()
colored_sq(g, 100, 90, 'brown', 'silver', 'gold', 'blue')
