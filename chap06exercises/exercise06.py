def move(turtle, dis, ang):
    turtle.forward(dis)
    turtle.right(ang)
    turtle.forward(dis)
    turtle.right(ang)
    turtle.forward(dis)
    turtle.right(ang)
    turtle.forward(dis)
    turtle.right(ang)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)
