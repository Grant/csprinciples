def posible_to_drive(tc, al, mpg):
    tank_capacity = tc
    amount_left = al
    num_gallons = tank_capacity * amount_left
    miles_per_gallon = mpg
    num_miles = num_gallons * miles_per_gallon
    return(num_miles)

print(posible_to_drive(10, 0.25, 32))

