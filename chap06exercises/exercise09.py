def square(turtle, l):
    turtle.forward(l)
    turtle.right(90)
    turtle.forward(l)
    turtle.right(90)
    turtle.forward(l)
    turtle.right(90)
    turtle.forward(l)
    turtle.right(90)

from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
square(malik, 100)           # draw a square with malik

