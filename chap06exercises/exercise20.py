def time_plus(h, m, a1, a2):
    nh = h+a1
    nm = m+a2
    if nm > 60:
        nm = nm - 60
        nh = nh + 1
    if nh > 12:
        nh = nh - 12
    new_hour = nh
    new_minute = nm
    if new_minute > 10:
        return(str(new_hour) + ":" + str(new_minute))
    else:
        return(str(new_hour) + ":0" + str(new_minute))
print(time_plus(12, 45, 2, 16,))
