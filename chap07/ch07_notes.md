## Repeating Steps
# Learning Objectives:
    -Use a for loop to repeat code.
    -Use range to create a list.
  -Steps often have to be repeated
  -We could stir something 50 times, but would get tired if we had to stir it 1,000 times
  -There is a way to get computers to repeat steps in a program with a loop or iteration.
## Repeating with Numbers
  -A for loop is a type of way to repeat a statement or set of statements.
  -A for loop uses a variable and mate the other variables take on each of the values in a list of numbers one at a time.
  -A list holds values in order.
  -The start of a for loop (for number in things_to_add:) always ends with a ":".
  -The code in a for loop is indented, and it ends when the indentation stops.
  -There has to be at least 1 statement in the body of a loop.
## What is a List?
  -A list holdes items in order.
  -A list in python is inclosed in [].
  -A list can have values seperated lby commas, ex. [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].
  -A list has an order and each list item has a position in the list.
  -Programs should be writen with good variable name (names that make sense for the variable)
### Using Better Variable Names
  -If a variable name makes sense with something in the code (ex. sum when using +) then the variable name should be changed if the contex in the code changes (ex. if + changes to * then sum should change to product)
## The Range Function
  -The range function can be used to loop over a sequence of numbers.
  -If range is called with a single positive integer, it will generate all integer values from 0 to one under the number that it was passed and assign them one at a time to the loop variable.
  -If 2 integer values are imputed, it will go from 0 to 1 under the second integer value, and will start with the first integer.
## There's a Pattern Here!
  -Theres a pattern in these programs, which is common when processing data, called the Accumlator Patern.
  -The first program in the chapter accumulated the values into the variable sum.
  -In recent programs in the chapeter, a product was accumulated into a variable named product.
  -There are 5 steps. They are as follows:
     1. Set the accumulator variable to its inital value. This is the value that one would want if there is no data to be processed
     2. Get all data to be processed
     3. Step through all data using a for loop so the variable takes on each value in the data.
     4. Combine each peice of data into the accumulator.
     5. Do something with the result
## Adding Print Statements
  -The goal of this stage of learning is to learn what code does. It should give us the ability to predict what going to happen from code, or know the value of the variables.
  -Predict the value of variables in exercises.

