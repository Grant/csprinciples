def average_between_product_and_sum(start_of_list, end_of_list):
    product = 1
    sum = 0
    numbers = range(start_of_list, end_of_list + 1)
    for n in numbers:
        product = product * n
        sum = sum * n
    average = sum + product
    average = average / 2
    return int(average)
print(average_between_product_and_sum(1, 10))
