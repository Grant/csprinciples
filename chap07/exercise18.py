def product_all_even_in_2_to_passed(passed_num):
    product = 1
    numbers = range(2, passed_num + 1, 2)
    for n in numbers:
        product = product * n
    return product
print(product_all_even_in_2_to_passed(20))
