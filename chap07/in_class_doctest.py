def letter_combiner(let):
    """
      >>> letter_combiner(['H', 'i'])
      Hi
    """
    temp_string = ""
    for letter in let:
        temp_string = letter
    return temp_string

if __name__ == "__main__":
    import doctest
    doctest.testmod()
