def product_even_sum_odd_diference(n):
    odds = range(1, n + 1, 2)
    evens = range(2, n + 1, 2)
    sum = 0
    product = 1
    for num_odd in odds:
        sum = sum + num_odd
    for num_even in evens:
        product = product * num_even
    if sum > product:
        final = sum - product
        return final
    else:
        final = product - sum
        return final
print(product_even_sum_odd_diference(10))
