def sum_evens(to_num):
    sum = 0
    numbers = range(0, to_num, 2)
    for number in numbers:
        sum = sum + number
    return(sum)

print(sum_evens(21))
