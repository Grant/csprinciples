numbers = range(1, 11)
for number in numbers:
    solution = number * number
    print(f"{number} * {number} = {solution}")
