* asymmetric ciphers
  - A cipher that uses a pair of keys, a public key and a private, or much harder to access, key.
* authentication
  - Someting that proves someone or something has access to something.
* bias
  - Having better opinions of some people based on their skin color, sexual alingment, gender, sex, or any other number of reasons that have nothing to do with the results of their work. 
* Certificate Authority (CA)
  - Someone or something that keeps track of digital certificates by storing them, signing them, and issuing them.
* citizen science
  - Research that uses public participation.
* Creative Commons licensing
  - A copyright liscence that lets people share their copyrighted content to other people.
* crowdsourcing
  - Gaining information about something by asking a very large group of people abouth that.
* cybersecurity
  - The main way to protect yourself and your computer from getting harmed online, be it protecting from viruses or websites that steal your personal information.
* data mining
  - Where people use computer to gather large amounts of data to use for something.
* decryption
  - Taking an encrypted file and puting it back in it's original form.
* digital divide
  - Some people having lots of acess to digital tech while others dont have any access to that digital tech.
* encryption
  - Scrambeling code to make it hard for things (such as malware) that don't have pemision to access the file or data to be able to use that file or data.
* intellectual property (IP)
  - Something that you can aply for copyright.
* keylogging
  - Recording keystrokes on a computer.
* malware
  - A harmful type of file with a trigger and package that can gather your personal data or stop your computer from working properly.
* multifactor authentication
  - A process to check if someone or something can access something that requires multipul steps to be completed before that thing can be accessed.
* open access
  - Free access to something.
* open source
  - Something, usualy software, that give the user permision to change and study the thing the user is using.
* free software
  - Software that respects the user's freedom and their comunity.
* FOSS
  - A blanket term for free software and open source software.
* PII (personally identifiable information)
  - Information that can let people identify who someone is personaly.
* phishing
  - Sending an email and saying that you are a reputable entity to try to gain personal information.
* plagiarism
  - Stealing someone else's work and claiming that the work is your own.
* public key encryption
  - Encrypting something and making one of the keys public
* rogue access point
  - A device that is not aproved by a system admin but is on the system anyways.
* targeted marketing
  - Marketing a product to a spesific group of people more often than other groups of people.
* virus
  - A type of maleware that usualy has a direct, harmful effect on the computer it is on.
