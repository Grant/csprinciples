# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The program makes a game for the person using the program to play. In the game you play as a player and you are running away from the robots. If robots run into each other or into junk, they become junk. If all the robots become junk, then the player wins. If the player runs into a robot or a piece of junk, the player looses.

2. Describes what functionality of the program is demonstrated in the
   video.
> The video shows the movement mechanic, where the player uses a button on their keyboard to move the player. It also shows the teleport function, which allows the player to go to a random spot on the map, and it shows what happens when you loose.

3. Describes the input and output of the program demonstrated in the
   video.
> The program takes a key press as an input, and and moves the player and robots as an output based on the key input.

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> def place_robots(numbots):
    global robots

    robots = []

    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0, 63)
        robot.y = random.randint(0, 47)
        robot.junk = False
        robot.shape = Box((10*robot.x, 10*robot.y), 10, 10)
        robots.append(robot)

2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
> def move_robots():
    global robots

    for robot in robots:
        if not robot.junk:
            if robot.x > player.x:
                robot.x -= 1
            elif robot.x < player.x:
                robot.x += 1

            if robot.y > player.y:
                robot.y -= 1
            elif robot.y < player.y:
                robot.y += 1

            move_to(robot.shape, (10 * robot.x, 10 * robot.y))

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The name of the lis is "robots"

2. Describes what the data contained in the list represent in your
   program
> Each time a robot is placed, it is added to the list. The robots on that list are then reffered to fir movement and to check the postion of each robot.

3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> The list makes it so that it is easy to move all the robots at once with a for loop, and change how many robots there are. It would be much harder to be able to manage multiple robots and change how many robots there are without the list

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>def check_collisions():
    global finished, robots

    # Handle player crashes into robot
    if collided(player, robots):
        finished = True
        Text("You've been caught!", (320, 240), size=36)
        sleep(3)
        return

    # Handle robots crashing into each other
    surviving_robots = []

    for robot in robots:
        if collided(robot, junk):
            continue

        jbot = robot_crashed(robot)

        if not jbot:
            surviving_robots.append(robot)
        else:
            remove_from_screen(jbot.shape)
             jbot.junk = True
                jbot.shape = Box(
                    (10 * jbot.x, 10 * jbot.y), 10, 10, filled=True
                )
                junk.append(jbot)

        robots = []

        for robot in surviving_robots:
            if not collided(robot, junk):
                robots.append(robot)

        if not robots:
            finished = True
            Text("You win!", (160, 240), size=36)
            sleep(3)
            return 

2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> while not game_over:
    finished = False
    junk = []

    place_robots(numbots)
    safely_place_player()

    while not finished:
        move_player()
        move_robots()
        check_collisions()

    if numbots < 17:
        numbots *= 2
        clear_screen()
    else:
        game_over = True

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> The prosedure checks if an object (a robot, pile of junk, or the player) has run into another object and executes based on what objects ran into each other

4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> The program starts by checking if the player has run into a robot, and ends the game if they have. Afterwards, it checks if a robot has run into another robot and removes both robots from the screen, and places junk in the placd of the robots. It then checks if all the robots have been turned into junk, and if they all have, tells the player that they have won.

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>

> Second call:
>

2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
> 

> Condition(s) tested by the second call:
> 

3. Identifies the result of each call
>
> Result of the first call:
> 

> Result of the second call:
> 
