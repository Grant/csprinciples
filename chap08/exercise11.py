product = 1  # Start with multiplication identity
numbers = 1
while numbers < 11:
    product = product * numbers
    numbers += 1
print(product)
