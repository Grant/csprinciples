bottom = input("How long do you want the bottom base of a right triangle to be? ")
bottom = int(bottom)
line = 1
while line < bottom + 1:
    print("*" * line)
    line += 1
