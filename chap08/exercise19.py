length = input("What is the length of the square? ")
length = int(length)
line = 1
spaces = length - 2
while line < length + 1:
    if line == 1:
        line += 1
        print("*" * length)
    else:
        if line // length == 1:
            line += 1
            print("*" * length)
        else:
            line += 1
            print("*" + " " * spaces + "*")
