## Loops - For and While
Learning Objectives:
- Introduce the concept of a `while` loop, which will repeat the body of the loop when the while statement is true.
- Introduce the concept of a logicla expresion, which can be true or false
- Introduce the concept of a infinite loop, which never ends
- Compare and contrast `while` and `for` loops.
## Example For Loop
- For loop has been used to repeat the body a know number of times.
- The body of a loop is the indented code under the loop.
Test code in book:
```
for counter in range(1, 10):
    print(counter
```
## Introducing the While Loop
- Another way to repeat statements is with the `while` loop.
- ```while``` loops repeat the body of the loop as long as a logical expresion is true
- A logical expresion is either true or false (ex. `5 < x`)
- While loops are for when you don't know how many times to execute the loop (generaly)
- The code in the book (below) asks for a number imput, and will only stop whe a negative imput is given.
Code from book:
```
sum = 0
count = 0
message = "Enter a positive integer, or a negative number to stop"
value = input(message)

while int(value) > 0:
    print("You entered " + value)
    sum = sum + int(value)
    count = count + 1
    value = input(message)

print("The sum is: " + str(sum) +
      " the average is: " + str(sum / count))
```
## Infinite Loops
- It is simple to get a compute to repeat steps, but it can be harder to stop a computer once it has started
- A while loop will execute as long as a logical statement is true, but what happens when the statement is always true, like `1 == 1`
- It will loop forever
- Because the statement is always true, then the loop never ends
- That is called an infinite loop, because it doesn't end unless it is forced to stop
- ``==`` tests if somthing is equal to something, while `=` assings a value to something
## Continuing with a While Loop
- It's easy to get a computer to loop a set number of times with a for loop using a range
- It is also possible to loop a certain number of times with a while loop
- One example is having a counter count up from 10, with the loop being `while counter < 11`, and add a line of code at the end of the body that says `counter += 1`, and it ends at 10 because `11 == 11` is true, which means that 11 = 11, which means that `11 < 11` is false.
## Side by Side Comparison of a For Loop and a While loop
- The `for` loop sets up a `counter`, and does something with the `counter` in the body of the loop
- The `while` loop also sets up a `counter`, and changes the value of the `counter` but does not use it otherwise in the loop.
- Both have diferent uses, and are prone to diferent errors
## Looping When We Don't Know When We'll Stop
- `while` loops are usualy used in the case where the number of times the loop has to occur is unknown
- There is a special process for these `while` loops
- There is a way to trace the number of repetition in a `while` loop (it was late and I didnt't have headphones I could use to watch the video)
## Nested For Loops
- The body of a loop can include another loop
## How Many Times Does the Inner Loop Execute?
- You can figure out how many times the inner loop is run based on how many times the inner and outer loop runs
