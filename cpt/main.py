from random import randint 
import possible_problems
wordnum = randint(0,9)
word = possible_problems.solutions(wordnum)
incorect_guesses_left = 6
correct_guesses = 0
correct_letters = []
guessed_letters = []
for i in word:
  print("_")
game_started = True
while game_started == True:
  strikes = 0
  if wordnum == 2 and guessed_letters != " " and len(guessed_letters) > 1:
    print("There are spaces, enter one first before you actually guess.")
  print(f"So far, the letters you have found are {correct_letters}. You have {len(word) - len(correct_letters)} letters remaining!")
  guessed_letter = input("Put a letter here: ")
  for i in guessed_letters:
    if guessed_letter == i:
      guessed_letter = input("Sorry, you already guessed that letter. Put a letter that you haven't guessed yet here: ")
  while len(guessed_letter) > 1:
    guessed_letter = input("Please enter ONE letter here: ")
  for i in word:
    if guessed_letter == i:
      print(f"{guessed_letter} is correct!")
      correct_letters += guessed_letter
      correct_guesses += 1
    else:
       strikes += 1
  guessed_letters += guessed_letter
  if strikes == len(word):
    incorect_guesses_left -= 1
    print(f"Sorry, {guessed_letter} is not part of the word. You have {incorect_guesses_left} incorect guesses left.")
  if correct_guesses == len(word):
    print(f"You are correct! The word was '{word}'.")
    game_started = False
  if incorect_guesses_left == 0:
    print(f"You lose, better luck next time! The word was '{word}'.")
    game_started = False
  if game_started == False:
    try_again = input("Would you like to try again? y or n: ")
    while try_again != "y" and try_again != "n" or len(try_again) > 1:
      try_again = input("Please enter y or n here: ")
    if try_again == "y":
      word = possible_problems.solutions(randint(0,9))
      incorect_guesses_left = 7
      correct_guesses = 0
      correct_letters = ""
      guessed_letters = ""
      game_started = True
      for i in word:
        print("_")
    else:
      game_started = False
    
