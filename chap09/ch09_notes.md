## Chapter 9 - Repeating Steps with Strings
### Using Repetition with Strings
Learning Objectives:
- Show how the accumulator patern works for strings
- Show how to reverse a string
- Show how to mirror a string
- Show how to use a while loop to modify a string
- Content of section of chapter:
- Python can use string (a colection of letters, numbers, and other charecters)
- Python `for` loops know how to step through letters
- Addition with strings will merge the strings
- The accumulator patern works the same way
- The 5 steps of the accumulator patern are as follows:
1. Set the accumulator variable to its initial value
2. Get all the data to be processed
3. Step through all the data using a `for` loop so that the variable takes on each value in the data
4. Combine each piece of the data into the accumulator
5. Do something with the result
### Reversing text
- Changing the order of `new_string + letter` to `letter + new_string` drasticly changes the result
### Mirroring Text
- If you add a letter to both sides of a new string that is being made, if the original string isd "eh", the new string will be "heeh"
- The accumulator does not have to be an empty string
### Modifying Text
- It is posible to replace charecters in strings with other charters
- It is possible to scramble and un-scramble strings    
