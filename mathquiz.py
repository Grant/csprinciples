from random import randint

quiz = f"How many questions would you like? "
questions = int(input(quiz))

correct = 0

for q in range(questions):
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    question = f"What is {num1} times {num2}? "
    answer= num1 * num2
    response = int(input(question))
    
    if answer == response:
        print("Thats right - well done.")
        correct += 1
    else:
        print(f"no, I'm afraid the answer is {answer}")

if correct == questions:
    print(f"You got all {questions} right! Amazing!")
elif correct > 5:
        print(f"I asked you {questions} question. You got {correct} of them right. \nWell done!")
elif correct > 0:
    print(f"You got {correct} right, study some more and try again.")
else:
    print(f"Dang, you got 0 right. You need to study a lot more")
